package 
{
	import flash.desktop.NativeApplication;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import starling.core.Starling;
	
	[SWF(width="800", height="600", frameRate="60", backgroundColor="#000000")]
	public class Application extends Sprite
	{
		protected var _starling : Starling;
		protected var _viewport : Rectangle;
		protected var _preloader : DisplayObject;
		
		public function Application(Entry : Class, stats : Boolean = false, preloader : DisplayObject = null )
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(Event.DEACTIVATE, _desactivate);
			
			// touch or gesture?
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			_viewport = new Rectangle(0, 0, stage.fullScreenWidth, stage.fullScreenHeight);
			
			if (preloader)
			{
				_preloader = preloader;
				_preloader.width  = _viewport.width;
				_preloader.height = _viewport.height;
				
				if (_preloader is Bitmap)
					Bitmap(_preloader).smoothing = true;
				
				_preloader.cacheAsBitmap = true;
				addChild(_preloader);
			}
			
			_starling = new Starling(Entry, stage, _viewport);
			_starling.simulateMultitouch  = false;
            _starling.enableErrorChecking = false;
			_starling.showStats = stats ;
			
			_starling.stage3D.addEventListener(Event.CONTEXT3D_CREATE, _start); 
			
			NativeApplication.nativeApplication.addEventListener(Event.ACTIVATE, _activate);
		}
		
		private function _start(e:Event):void 
		{
			if (_preloader)
			{				
				removeChild(_preloader);
				_preloader = null;
			}
			
            _starling.start();
			_ready();
		}
		
		protected function _ready() : void
		{
			//override
		}
		
		private function _activate(e:Event):void 
		{
			_starling.start();
		}
		
		protected function _desactivate(e:Event):void 
		{
			_starling.stop();
			
			// auto-close
			NativeApplication.nativeApplication.exit();
		}
	}
}