package examples 
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.TouchEvent;
	/**
	 * ...
	 * @author pautay
	 */
	
	import as3isolib.core.IIsoDisplayObject;
	import as3isolib.display.IsoView;
	import as3isolib.display.primitive.IsoBox;
	import as3isolib.display.scene.IsoGrid;
	import as3isolib.display.scene.IsoScene;
	import as3isolib.geom.IsoMath;
	import as3isolib.geom.Pt;
	
	import eDpLib.events.ProxyEvent;
	
	public class Example07 extends Sprite
	{
		private var box:IsoBox;
		private var scene:IsoScene;
		private var _tween : Tween;
		
		public function Example07 ()
		{
				scene = new IsoScene();
				
				var g:IsoGrid = new IsoGrid();
				g.showOrigin = false;
				Config.STAGE.addEventListener(MouseEvent.CLICK, grid_mouseHandler);
				scene.addChild(g);
				
				box = new IsoBox();
				box.setSize(25, 25, 25);
				scene.addChild(box);
				
				var view:IsoView = new IsoView();
				view.clipContent = false;
				view.setSize(Config.WIDHT, Config.HEIGHT);
				view.addScene(scene);
				addChild(view);
				
				scene.render();
				
				view.showBorder = true;
		}
		
		private function grid_mouseHandler (evt:MouseEvent):void
		{					
		   var mEvt:MouseEvent = MouseEvent(evt);
			var pt:Pt = new Pt(mEvt.localX - Config.STAGE.stageWidth / 2, evt.localY - Config.STAGE.stageHeight/2 );
			
			pt = IsoMath.screenToIso(pt);
			
			if (_tween)
			{
				_tween.reset(box, 0.5);
			}
			else
			{
				_tween = new Tween(box, 0.5);
				_tween.onComplete = gt_completeHandler;
			}
			
			_tween.animate("x", pt.x);
			_tween.animate("y", pt.y);
			
			if(!Starling.juggler.contains(_tween))
				Starling.juggler.add(_tween);
			
			if (!hasEventListener(EnterFrameEvent.ENTER_FRAME))
					this.addEventListener(EnterFrameEvent.ENTER_FRAME, enterFrameHandler);
		}
		
		private function gt_completeHandler ():void
		{
				this.removeEventListener(EnterFrameEvent.ENTER_FRAME, enterFrameHandler);
		}
		
		private function enterFrameHandler (evt:EnterFrameEvent):void
		{
			scene.render();
		}
	}
}