package 
{
	import examples.Example01;
	import examples.Example02;
	import examples.Example03;
	import examples.Example04;
	import examples.Example05;
	import examples.Example06;
	import examples.Example07;
	import examples.Example08;
	import examples.Example09;
	import starling.core.Starling;
	
	/**
	 * ...
	 * @author pautay
	 */
	public class Main extends Application 
	{
		
		public function Main()
		{
			Config.STAGE = stage;
			//super(Example01, true);
			//super(Example02, true);
			//super(Example03, true);
			//super(Example04, true);			
			//super(Example05, true);
			//super(Example06, true);
			//super(Example07, true);
			//super(Example08, true);
			super(Example09, true);
			
			trace("width : " + Starling.current.viewPort.width + ", height : " + Starling.current.viewPort.height);
		}
	}	
}